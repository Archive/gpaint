#ifndef __DEBUG_H__
#define __DEBUG_H__
/*
    Copyright 2000  Li-Cheng (Andy) Tai
    Copyright 2002  Michael A. Meffie III

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
    Debugging macros. Define GPAINT_DEBUG to activate, either by
    adding -DGPAINT_DEBUG in the Makefile CFLAGS variable, or by
    adding #define GPAINT_DEBUG to the module that includes this
    file.

    The macros are:
     debug_fn()           -- print the name of the current function
     debug(msg)           -- print the message string
     debug1(format,x)     -- print one param using the format string
     debug2(format,x,y)   -- print two params using the format string
     debug2(format,x,y,z) -- print three params using the format string

 */
#include <stdio.h>

#ifdef GPAINT_DEBUG
# define DEBUG_HDR()  fprintf(stderr, __FILE__ " (%d) : ", __LINE__);
# define debug_fn()   DEBUG_HDR(); fprintf(stderr, __PRETTY_FUNCTION__ "()\n");
# define debug(msg)   DEBUG_HDR(); fprintf(stderr, "%s\n", msg);
# define debug1(format,x) DEBUG_HDR(); fprintf(stderr, format ## "\n", (x));
# define debug2(format,x,y) DEBUG_HDR(); fprintf(stderr, format ## "\n", (x),(y));
# define debug3(format,x,y,z) DEBUG_HDR(); fprintf(stderr, format ## "\n", (x),(y),(z));
#else
# define debug_fn()
# define debug(msg)
# define debug1(format,x)
# define debug2(format,x,y)
# define debug3(format,x,y,z)
#endif

#endif
