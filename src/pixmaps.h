

#ifndef __PIXMAPS_H__
#define __PIXMAPS_H__

#ifdef __cplusplus
extern "C"
{
#endif

extern const char *about_pict_xpm[];
extern const char *arcOp_xpm[];
extern const char *arrow_xpm[];
extern const char *babygnu_l_xpm[];
extern const char *boxOp_xpm[];
extern const char *brushOp_xpm[];
extern const char *clineOp_xpm[];
extern const char *curveOp_xpm[];
extern const char *dotPenOp_xpm[];
extern const char *dynPenOp_xpm[];
extern const char *eraseOp_xpm[];
extern const char *fboxOp_xpm[];
extern const char *ffillOp_xpm[];
extern const char *ffreehandOp_xpm[];
extern const char *filled_xpm[];
extern const char *fillOp_xpm[];
extern const char *fovalOp_xpm[];
extern const char *fpolyOp_xpm[];
extern const char *freehandOp_xpm[];
extern const char *lassoOp_xpm[];
extern const char *lineOp_xpm[];
extern const char *ovalOp_xpm[];
extern const char *paintA_xpm[];
extern const char *paintB_xpm[];
extern const char *paintC_xpm[];
extern const char *paintD_xpm[];
extern const char *paintE_xpm[];
extern const char *paintF_xpm[];
extern const char *paintG_xpm[];
extern const char *paintH_xpm[];
extern const char *paintI_xpm[];
extern const char *paintJ_xpm[];
extern const char *paintK_xpm[];
extern const char *paintL_xpm[];
extern const char *paintM_xpm[];
extern const char *paintN_xpm[];
extern const char *paintO_xpm[];
extern const char *paintP_xpm[];
extern const char *paintQ_xpm[];
extern const char *paintR_xpm[];
extern const char *paintS_xpm[];
extern const char *paintT_xpm[];
extern const char *pencilOp_xpm[];
extern const char *polyOp_xpm[];
extern const char *rayOp_xpm[];
extern const char *selectOp_xpm[];
extern const char *selpolyOp_xpm[];
extern const char *smearOp_xpm[];
extern const char *sprayOp_xpm[];
extern const char *textOp_xpm[];
extern const char *tfillOp_xpm[];
extern const char *unfilled_xpm[];


#ifdef __cplusplus
}
#endif

#endif

